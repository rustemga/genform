let genForm = document.forms.formElem;

//Phone number input field validation
function checkPhoneInput(value) {

    if (/^\d+$/.test(value[value.length - 1]) && value.length <= 18) {

        if (value[0] != "+") {
            genForm.phone.value = "+" + value;
        }
        if (genForm.phone.value.length == 2) {
            let arr = genForm.phone.value.split("");
            arr[1] = 7;
            value = arr.join("");
            genForm.phone.value = value + " (";
        }
        if (genForm.phone.value.length == 7) {
            genForm.phone.value = value + ") ";
        }

        if (genForm.phone.value.length == 12) {
            genForm.phone.value = value + "-";
        }
        if (genForm.phone.value.length == 15) {
            genForm.phone.value = value + "-";
        }

    } else {
        let index = value.length - 1;
        let valueArr = value.split('')
        valueArr.splice(index, 1)
        genForm.phone.value = valueArr.join('');
        if (value == "") {
            genForm.phone.value = "+";
        }
    }
};

//Show input error messages
function showError(input, message) {
    const formControl = input.parentElement;
    formControl.className = 'form-control error';
    const small = formControl.querySelector('small');
    small.innerText = message;
}

//show success colour
function showSucces(input) {
    const formControl = input.parentElement;
    formControl.className = 'form-control success';
}

//check email is valid
function checkEmail(input) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(input.value.trim())) {
        showSucces(input);
        return true;
    } else {
        showError(input, 'Email is not invalid');
        return false;
    }
}

//check phone is valid /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/
function checkPhone(input) {
    const re = /^\+?([7]{1})\)?[ ]?[(]?([0-9]{3})\)?[)]?[ ]?([0-9]{3})[-]?([0-9]{2})[-]?([0-9]{2})$/;
    if (re.test(input.value.trim())) {
        showSucces(input);
        return true;
    } else {
        showError(input, 'Phone number is not valid');
        return false;
    }
}


//Form submit
genForm.addEventListener('submit', function (e) {

    e.preventDefault();

    let formData = new FormData(genForm);

    let xhttp = new XMLHttpRequest();
    let data = {
        email: formData.get("email"),
        phone: formData.get("phone")
    };

    if (checkPhone(genForm.phone) && checkEmail(genForm.email)) {
        xhttp.onload = function () {
            console.log(xhttp.responseText);
        }
        xhttp.open("POST", "app/amo/amo.php");
        xhttp.setRequestHeader("Content-type", "application/json", true);
        xhttp.send(JSON.stringify(data));
    }else{
        console.log("Form not sended!");
    }
});

function showPopup(){
    let form = document.getElementById('form-popup');
    form.style.display = "block";
}

function closeForm(){
    let form = document.getElementById('form-popup');
    form.style.display = "none";
}