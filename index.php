<!DOCTYPE html>
<html lang='ru'>

<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <link rel="stylesheet" href="app/css/style.css">
    <title>Добро пожаловать!</title>
</head>

<body>
    <div class="btn-wrapper">
        <button onclick="showPopup()">Кликни</button>
    </div>
    <div class="form-wrapper" id="form-popup">
        <div class="form-popup">
            <img src="app/img/cros.png" alt="close" onclick="closeForm()">
            <div class="form-img">
                <h1>Получите набор файлов для руководителя:</h1>
                <div class="form-docs">
                    <img src="app/img/doc.png" alt="file">
                    <img src="app/img/xls.png" alt="file">
                    <img src="app/img/pdf.png" alt="file">
                    <img src="app/img/pdf.png" alt="file">
                    <img src="app/img/pdf.png" alt="file">
                    <img src="app/img/pdf.png" alt="file">
                    <img src="app/img/pdf.png" alt="file">
                </div>
                <img src="app/img/docs2.png" alt="docs">
            </div>
            <div class="form-fields">
                <form name="formElem" method="post">
                    <div class="form-control">
                        <label for="email">Введите Email для получения файлов:</label>
                        <input type="text" name="email" placeholder="E-mail">
                        <small>Error Message</small>
                    </div>
                    <div class="form-control">
                        <label for="phone">Введите телефон для подтверждения доступа:</label>
                        <input type="text" name="phone" placeholder="+7 (000) 000-00-00" onkeyup="checkPhoneInput(this.value)">
                        <small>Error Message</small>
                    </div>
                    <div class="form-control">
                        <button>Скачать файлы <img src="app/img/hand.png" alt="hand"></button>
                    </div>
                    <div class="formats">
                        <div class="formats-item">
                            <p>PDF 4,7 MB</p>
                        </div>
                        <div class="formats-item">
                            <p>DOC 0,8 MB</p>
                        </div>
                        <div class="formats-item">
                            <p>XLS 1,2 MB</p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="app/js/script.js"></script>
</body>

</html>